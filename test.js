var should = require('should');

var csvcsv = require('./index');

describe('defaults', function () {

  /*
   * Seems dumb but we rely on the defaults for several tests.
   */

  it('checking defaults', function () {

    should(csvcsv.defaults.max_depth).equal(3);
    should(csvcsv.defaults.level_marker).equal('.');
    should(csvcsv.defaults.strategy).equal(false);

  });

});

describe('escaper()', function () {

  it('Hello "world"!', function () {

    var escape = csvcsv.escaper('"', '\\');
    var result = escape('Hello "world"!');

    should(result).equal('Hello \\"world\\"!')

  });

});

describe('demangle()', function () {

  it('flat', function () {

    var data = [
      { adams: 42 },
      { satan: 666 }
    ];

    var result = csvcsv.demangle(data, csvcsv.defaults);
    var expected = {
      headers: [ 'adams', 'satan' ],
      rows: [
        { adams: 42 },
        { satan: 666 }
      ]
    };

    should(result).eql(expected);

  });

  it('nested', function () {

    var data = [
      {
        adams: 42
      },
      {
        value: 666,
        satan: {
          value: 'more_complex'
        }
      }
    ];

    var result = csvcsv.demangle(data, csvcsv.defaults);
    var expected = {
      headers: [ 'adams', 'value', 'satan.value' ],
      rows: [
        {
          'adams': 42
        },
        {
          'value': 666,
          'satan.value': 'more_complex'
        }
      ]
    };

    should(result).eql(expected);

  });

});

describe('demangle_item()', function () {

  it('flat', function () {

    var data = {
      answer: 42
    };

    var result = csvcsv.demangle_item(data, csvcsv.defaults);
    var expected = {
      headers: [ 'answer' ],
      row: { answer: 42 }
    };

    should(result).eql(expected);

  });

  it('array', function () {

    var data = {
      values: [1, 2, 3, 4, 5]
    };

    var result = csvcsv.demangle_item(data, csvcsv.defaults);
    var expected = {
      headers: [
        'values.0',
        'values.1',
        'values.2',
        'values.3',
        'values.4'
      ],
      row: {
        'values.0': 1,
        'values.1': 2,
        'values.2': 3,
        'values.3': 4,
        'values.4': 5
      }
    };

    should(result).eql(expected);

  });

  it('1 level deep', function () {

    var data = {
      answer: {
        value: 42
      }
    };

    var result = csvcsv.demangle_item(data, csvcsv.defaults);
    var expected = {
      headers: [ 'answer.value' ],
      row: { 'answer.value': 42 }
    };

    should(result).eql(expected);

  });

  it('2 levels deep', function () {

    var data = {
      answer: {
        value: {
          value: 42
        }
      }
    };

    var result = csvcsv.demangle_item(data, csvcsv.defaults);
    var expected = {
      headers: [ 'answer.value.value' ],
      row: { 'answer.value.value': 42 }
    };

    should(result).eql(expected);

  });

  it('3 levels deep (above max_depth)', function () {

    var data = {
      answer: {
        value: {
          value: {
            value: 42
          }
        }
      }
    };

    var result = csvcsv.demangle_item(data, csvcsv.defaults);
    var expected = {
      headers: [],
      row: {}
    };

    should(result).eql(expected);

  });

});

describe('format_line()', function () {

  it('single value', function () {

    var headers = ['answer'];
    var line = {
      answer: 42
    };

    var result = csvcsv.format_line(line, headers, csvcsv.defaults);
    var expected = '42';

    should(result).eql(expected);

  });

  it('single value (2)', function () {

    var headers = ['answer'];
    var line = {
      age: 30,
      answer: 42
    };

    var result = csvcsv.format_line(line, headers, csvcsv.defaults);
    var expected = '42';

    should(result).eql(expected);

  });

  it('double value', function () {

    var headers = ['age', 'answer'];
    var line = {
      age: 30,
      answer: 42
    };

    var result = csvcsv.format_line(line, headers, csvcsv.defaults);
    var expected = '30,42';

    should(result).eql(expected);

  });

});

describe('process()', function () {

  var a_json = require('./fixtures/a.json');

  it('fixtures/a.json', function () {

    var result = csvcsv.process(a_json.data);
    var expected = a_json.expected;

    should(result).eql(expected);

  });

});

describe('issues', function () {

  it('#1 - Duplicate columns', function () {

    var data = [
      { a: 1 },
      { a: 2 }
    ];

    var result = csvcsv.process(data);
    var expected = 'a\n1\n2';

    should(result).eql(expected);

  });


});
