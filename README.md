# csvcsv

*csvcsv* is a simple and well-tested library for
transforming JSON objects of arbitrary nesting level
into CSV data. It only targets Node.js for now.


### Example

```javascript
var fixtures = require('users');
var csvcsv = require('csvcsv');

csvcsv.process(fixtures.users); // returns CSV.
```


### Installation

```shell
% npm install csvcsv
```


### Usage

#### `csvcsv.process( data [, options ] )`

This method process data passed to it. Data should be
in the form of an array. The following options are
supported:

- `separator`: Field separator (defaults to `,`).
- `strategy`: Quoting strategy (defaults to `false`).
- `quote_character`: Quoting character (defaults to `"`).
- `escape_character`: Escape character (defaults to `\`).
- `headers`: Whether or not to include headers in output (defaults to `true`).
- `lines_separator`: Character used to separates lines in the output (defaults to `\n`).
- `max_depth`: Depth level up to which keys should be extracted (defaults to 3).
- `level_marker`: Character used to build column names for nested data (defaults to `.`).


### License

ISC © 2014 Acute Analytics / Benoit Myard <bmyard@acute.fr>
