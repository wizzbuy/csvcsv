var _ = require('underscore');

module.exports = {

  defaults: {

    // Field separator.
    separator: ',',

    // Quoting strategy (defaults to `false`).
    strategy: false,

    // Quoting character (defaults to `"`).
    quote_character: '"',

    // Escape character (defaults to `\`).
    escape_character: '\\',

    // Whether or not to include headers in output (defaults to `true`).
    headers: true,

    // Character used to separates lines in the output.
    lines_separator: '\n',

    // Depth level up to which keys should be extracted.
    max_depth: 3,

    // Character used to build column names for nested data.
    level_marker: '.'

  },

  strategies: {
    always: true,
    never: false,
    auto: function (key, val) {
      return _.isString(val);
    }
  },

  escaper: function (char, escape) {
    var re = new RegExp(char, 'g');
    return function (input) {
      return input.replace(re, escape + char);
    };
  },

  process: function (data, options) {
    options = _.defaults(options || {}, this.defaults);

    if (_.isString(options.strategy)) {
      options.strategy = this.strategies[options.strategy] || false;
    }

    var demangled = this.demangle(data, options);
    var output = this.format(demangled, options);

    return output;
  },

  demangle: function (data, options) {
    var self = this;

    var headers = [];
    var rows = [];

    _.each(data, function (item) {
      var child = self.demangle_item(item, options);

      headers = headers.concat(child.headers);
      rows = rows.concat(child.row);
    });

    return {
      headers: _.unique(headers),
      rows: rows
    };
  },

  demangle_item: function (item, options, prefix, depth) {
    var self = this;

    prefix = prefix || [];
    depth = depth || 1;

    if (depth > options.max_depth) {
      return;
    }

    var headers = [];
    var row = {};

    _.each(item, function (val, key) {
      if (_.isObject(val) || _.isArray(val)) {
        var child_prefix = [].concat(prefix, key);
        var child = self.demangle_item(val, options, child_prefix, depth + 1);

        if (!child) {
          return; // max_depth has been exceeded
        }

        headers = headers.concat(child.headers);
        row = _.extend(row, child.row);
      }
      else {
        if (prefix.length) {
          key = [].concat(prefix, key).join(options.level_marker);
        }

        headers.push(key);
        row[key] = val;
      }
    });

    return {
      headers: headers,
      row: row
    };
  },

  format: function (data, options) {
    var self = this;

    var output = [];

    if (options.headers) {
      output.push(data.headers.join(options.separator));
    }

    _.each(data.rows, function (row) {
      output.push(self.format_line(row, data.headers, options));
    });

    return output.join(options.lines_separator);
  },

  format_line: function (line, headers, options) {
    var self = this;

    var always = options.strategy === true;
    var never = options.strategy === false;

    var escape = this.escaper(options.quote_character,
      options.escape_character);

    var items = [];

    _.each(headers, function (header) {
      var cell = _.result(line, header);

      if (!never && (always || options.strategy(header, cell))) {
        cell = options.quote_character + escape(cell) + options.quote_character;
      }

      items.push(cell);
    });

    return items.join(options.separator);
  }

};
